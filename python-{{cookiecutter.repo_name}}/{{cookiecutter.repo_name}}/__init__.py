"""
{{ cookiecutter.project_short_description }}

.. moduleauthor:: {{cookiecutter.full_name}} <{{cookiecutter.email}}>

"""

__version__ = "{{cookiecutter.version}}"

{%- if cookiecutter.type == 'http' %}
# HTTP __init__.py
{%- elif cookiecutter.type == 'rmq' %}
# RMQ __init__.py
{%- else %}
# python bare-ish __init__.py
{%- endif %}
